nope: nope.o oOo.o
	cc -o nope nope.o oOo.o

nope.o: nope.c
	cc -O2 -I. -o nope.o -c nope.c

oOo.o: oOo/oOo.c
	cc -O2 -I. -o oOo.o -c oOo/oOo.c

clean:
	rm -f nope nope.o oOo.o
