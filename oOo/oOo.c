#include <stdlib.h>
#include <oOo/oOo.h>

/* blowing bubbles in the aether */

/*
  ties the given oOo between n and p
*/
oOo_t *
_knot(oOo_t * n, oOo_t * oOo, oOo_t * p) {
  // fold oOo onto n/P
  oOo->n = n;
  oOo->p = p;

  // fold n/p into oOo
  n->p = oOo;
  p->n = oOo;

  // why knot?
  return oOo;
}

/*
  removes the given oOo, free()s its struct and returns the o-value
*/
void *
_free(oOo_t * oOo) {
  void * o = oOo->o;

  // fold the n/p pointers over each other
  oOo->n->p = oOo->p;
  oOo->p->n = oOo->n;

  // unchecked memory access aboves guarantees a free() ride
  free(oOo);
  return o;
}

/*
  allocates and initializes an oOo struct
*/
oOo_t *
_alloc(void * o) {
  oOo_t * oOo;

  // you can't have the pudding if you don't include the meat
  if(NULL == o) {
    return NULL;
  }

  // wizard casts a spell
  oOo = (oOo_t *) malloc(sizeof(oOo_t));
  if(NULL == oOo) {
    // might ruin your permanent record, especially if accessed
    return NULL;
  }

  // omfg, it worked!  put stuff in it!
  oOo->n = oOo;
  oOo->o = o;
  oOo->p = oOo;

  return oOo;
}

/*
  adds a node to a given oOo on the n side
*/
oOo_t *
oOo_add_n(oOo_t ** oOo, void * o) {
  oOo_t * n = _alloc(o);
  oOo_t * k;

  // pointer guards
  if(NULL == oOo || NULL == n) {
    return NULL;
  }

  // set kurrent
  k = *oOo;
  if(NULL == k) {
    // no kurrent; create it with n
    *oOo = k = n;
  }

  // tie n onto k's n side
  _knot(k->n, n, k);
  return n;
}

/*
  adds a node to a given oOo on the p side
*/
oOo_t *
oOo_add_p(oOo_t ** oOo, void * o) {
  oOo_t * p = _alloc(o);
  oOo_t * k;

  // pointer guards
  if(NULL == oOo || NULL == p) {
    return NULL;
  }

  k = *oOo;
  if(NULL == k) {
    // no kurrent; create it with p
    *oOo = k = p;
  }

  // tie p onto k's p side
  _knot(k, p, k->p);
  return p;
}

/*
  adds a node to a given oOo on the n side and moves the ring
*/
oOo_t *
oOo_tie_n(oOo_t ** oOo, void * o) {
  oOo_t * n = oOo_add_n(oOo, o);
  if(n != NULL) {
    *oOo = n;
  }
  return n;
}

/*
  adds a node to a given oOo on the p side and moves the ring
*/
oOo_t *
oOo_tie_p(oOo_t ** oOo, void * o) {
  oOo_t * p = oOo_add_p(oOo, o);
  if(p != NULL) {
    *oOo = p;
  }
  return p;
}

/*
  removes a node from the given oOo on the n side
*/
void *
oOo_sub_n(oOo_t ** oOo) {
  oOo_t * k;

  // need oOo pointers
  if(NULL == oOo || NULL == *oOo) {
    return NULL;
  }

  k = *oOo;
  if(k->n == k) {
    // self-integrity requires sacrifice
    *oOo = NULL;
  }

  // free n and return n->o
  return _free(k->n);
}

/*
  removes a node from the given oOo on the p side
*/
void *
oOo_sub_p(oOo_t ** oOo) {
  oOo_t * k;

  // need oOo pointers
  if(NULL == oOo || NULL == *oOo) {
    return NULL;
  }

  k = *oOo;
  if(k->p == k) {
    // self-integrity requires sacrifice
    *oOo = NULL;
  }

  // free p and return p->o
  return _free(k->p);
}

/*
  removes kurrent node from oOo and moves ring to n side
*/
void *
oOo_cut_n(oOo_t ** oOo) {
  oOo_t * k;

  // need oOo pointers
  if(NULL == oOo || NULL == *oOo) {
    return NULL;
  }

  k = *oOo;
  if(k->n == k) {
    // self-integrity requires sacrifice
    *oOo = NULL;
  }
  else {
    // move oOo to n side
    *oOo = k->n;
  }

  // free kurrent
  return _free(k);
}

/*
  removes kurrent node from oOo and moves ring to p side
*/
void *
oOo_cut_p(oOo_t ** oOo) {
  oOo_t * k;

  // need oOo pointers
  if(NULL == oOo || NULL == *oOo) {
    return NULL;
  }

  k = *oOo;
  if(k->p == k) {
    // self-integrity requires sacrifice
    *oOo = NULL;
  }
  else {
    // move oOo to p side
    *oOo = k->p;
  }

  // free kurrent
  return _free(k);
}

/*
  iterate each node in the given oOo on the n side calling c() with a given q
*/
void
oOo_each_n(oOo_t * oOo, void (*c)(oOo_t *, void *), void * q) {
  oOo_t * k = oOo;

  // papers, please
  if(NULL == k || NULL == c) {
    return;
  }

  do {
    c(k, q); // make execute
    k = k->n;
  } while(k != oOo);

  return;
}

/*
  iterate each node in the given oOo on the p side calling c() with a given q
*/
void
oOo_each_p(oOo_t * oOo, void (*c)(oOo_t *, void *), void * q) {
  oOo_t * k = oOo;

  // papers, please
  if(NULL == k || NULL == c) {
    return;
  }

  do {
    c(k, q); // make execute
    k = k->p;
  } while(k != oOo);

  return;
}

/*
  iterate each node n-ways, free()ing it and its ->o value
*/
void
oOo_free_n(oOo_t ** oOo, void (*vfree)(void *)) {
  if(oOo != NULL && *oOo != NULL) {
    oOo_t * k = *oOo;
    k->p->n   = NULL;
    while(k != NULL) {
      oOo_t * o = k;
      k         = k->n;
      if(vfree != NULL && o->o != NULL) {
        vfree(o->o);
      }
      free(o);
    }
    *oOo = NULL;
  }
  return;
}

/*
  iterate each node p-ways, free()ing it and its ->o value
*/
void
oOo_free_p(oOo_t ** oOo, void (*vfree)(void *)) {
  if(oOo != NULL && *oOo != NULL) {
    oOo_t * k = *oOo;
    k->n->p   = NULL;
    while(k != NULL) {
      oOo_t * o = k;
      k         = k->p;
      if(vfree != NULL && o->o != NULL) {
        vfree(o->o);
      }
      free(o);
    }
    *oOo = NULL;
  }
  return;
}
