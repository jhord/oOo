#ifndef __oOo_H__
#define __oOo_H__

typedef struct oOo oOo_t;
struct oOo {
  oOo_t * n;
  void  * o;
  oOo_t * p;
};

oOo_t * oOo_add_n(oOo_t **, void *);
oOo_t * oOo_add_p(oOo_t **, void *);
oOo_t * oOo_tie_n(oOo_t **, void *);
oOo_t * oOo_tie_p(oOo_t **, void *);

void *  oOo_sub_n(oOo_t **);
void *  oOo_sub_p(oOo_t **);
void *  oOo_cut_n(oOo_t **);
void *  oOo_cut_p(oOo_t **);

void    oOo_each_n(oOo_t *, void (*)(oOo_t *, void *), void *);
void    oOo_each_p(oOo_t *, void (*)(oOo_t *, void *), void *);

void    oOo_free_n(oOo_t **, void (*)(void *));
void    oOo_free_p(oOo_t **, void (*)(void *));

#endif
