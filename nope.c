#include <assert.h>
#include <stdio.h>
#include <oOo/oOo.h>

void dump_g(oOo_t *, void *);
void dump_r(oOo_t *, void *);
void dump_o(oOo_t *, void *);
void dump_O(oOo_t *, void *);
void dump_v(char *, char *);
void dump_e(void *, void *);

int
main(int argc, char ** argv) {
  oOo_t * pos = NULL;
  oOo_t * neg = NULL;

  /*
    adding allocates a new node and puts it into the '->n' or '->p' slot
    based on the called function.  the head pointer is not moved.
  */
  oOo_add_p(&pos, (void *) "pos 1");
  oOo_add_p(&pos, (void *) "pos 2");
  oOo_add_p(&pos, (void *) "pos 3");

  assert((char *) pos->o          == "pos 1");
  assert((char *) pos->p->o       == "pos 3");
  assert((char *) pos->p->p->o    == "pos 2");
  assert((char *) pos->p->p->p->o == "pos 1");

  assert((char *) pos->o          == "pos 1");
  assert((char *) pos->n->o       == "pos 2");
  assert((char *) pos->n->n->o    == "pos 3");
  assert((char *) pos->n->n->n->o == "pos 1");

  /*
    tieing allocates a new node and puts it into the '->n' or '->p' slot
    based on the called function.  the head pointer is moved to the new node.
  */
  oOo_tie_n(&neg, (void *) "neg 3");
  oOo_tie_n(&neg, (void *) "neg 2");
  oOo_tie_n(&neg, (void *) "neg 1");

  assert((char *) neg->o          == "neg 1");
  assert((char *) neg->n->o       == "neg 3");
  assert((char *) neg->n->n->o    == "neg 2");
  assert((char *) neg->n->n->n->o == "neg 1");

  assert((char *) neg->o          == "neg 1");
  assert((char *) neg->p->o       == "neg 2");
  assert((char *) neg->p->p->o    == "neg 3");
  assert((char *) neg->p->p->p->o == "neg 1");

  /*
    iterating is easy.  the call signature for the dumping function includes
    a (void *) to allow for passing state.  here we just use it as a simple
    label but it could be a pointer to a struct.  abuse your illusion.
  */
  oOo_each_n(pos, dump_o, (void *) "pos");
  oOo_each_p(neg, dump_o, (void *) "neg");

  /*
    sub performs the opposite action of add.
    head pointers are NULL when the list is empty.
  */
  while(pos != NULL) {
    char * value = (char *) oOo_sub_p(&pos);
    dump_v("pos", value);
  }

  /*
    tie performs the opposite action of cut.
    head pointers are NULL when the list is empty.
  */
  while(neg != NULL) {
    char * value = (char *) oOo_cut_p(&neg);
    dump_v("neg", value);
  }

  oOo_tie_p(&pos, (void *) "pos 1");
  oOo_free_n(&pos, NULL);
  oOo_tie_p(&pos, (void *) "pos 2");
  oOo_tie_p(&pos, (void *) "pos 3");
  oOo_free_n(&pos, NULL);
  oOo_free_n(&pos, NULL);

  oOo_add_n(&neg, (void *) "neg 1");
  oOo_free_n(&neg, NULL);
  oOo_add_n(&neg, (void *) "neg 2");
  oOo_add_n(&neg, (void *) "neg 3");
  oOo_free_n(&neg, NULL);
  oOo_free_n(&neg, NULL);

  return 0;
}

/*
  illustrates the call signature for a dumping function passed to
  oOo_each_*().
*/
void
dump_o(oOo_t * oOo, void * v_label) {
  char * label = (char *) v_label;
  char * value = (char *) oOo->o;

  dump_v(label, value);

  oOo_each_p(oOo, dump_O, (void *) value);
  oOo_each_n(oOo, dump_O, (void *) value);
  return;
}

/*
  illustrates the call signature for a dumping function passed to
  oOo_each_*(
    illustrates the call signature for a dumping function
    passed to oOo_each_*().
  ).
*/
void
dump_O(oOo_t * oOo, void * v_label) {
  char * label = (char *) v_label;
  char * value = (char *) oOo->o;

  dump_v(label, value);
  return;
}

/* all the way at the bottom */
void
dump_v(char * label, char * value) {
  printf("%s: %s\n", label, value);
  return;
}
