#include <stdio.h>
#include <oOo/oOo.h>

oOo_t ** push(oOo_t **, char *);
oOo_t ** pop (oOo_t **);
oOo_t ** dump_o(oOo_t **);
void     dump_v(oOo_t *, void *);

int
main(int argc, char ** argv) {
  oOo_t * stack;

  // enter the stacktrix
  pop(pop(pop(
    push(
      push(
        push(
          &stack,
          (void *) "stack 1"),
        (void *) "stack 2"),
      (void *) "stack 3")
  )));

  push(&stack, "stack 1");
  push(&stack, "stack 2");
  pop (&stack);
  push(&stack, "stack 3");
  pop (&stack);
  pop (&stack);

  return 0;
}

oOo_t **
push(oOo_t ** oOo, char * val) {
  /*
    pushes an item onto the stack thing.

    you can use either the 'n' or the 'p' slot to implement a stack but
    tie must be used to ensure the head pointer is always at the most
    recently added item.

    whatever slot you use, the opposite slot is used for pop.
  */
  oOo_tie_p(oOo, (void *) val);
  //oOo_tie_n(oOo, (void *) val);

  printf("push (%s)\n", val);
  return dump_o(oOo);
}

oOo_t **
pop(oOo_t ** oOo) {
  /*
    pops an item off the stack thing.
    
    since it doesn't return the value or allow you to pass in a callback,
    this is pretty useless.  for demonstration purposes only.

    be sure to cut from the opposite onto which you tie'd.
  */
  char * val = (char *) oOo_cut_n(oOo);
  //char *val = (char *) oOo_cut_p(oOo);

  printf("pop  (%s)\n", val);
  return dump_o(oOo);
}

oOo_t **
dump_o(oOo_t ** oOo) {
  printf("  n: ");
  oOo_each_n(*oOo, dump_v, NULL);
  printf("\n");

  printf("  p: ");
  oOo_each_p(*oOo, dump_v, NULL);
  printf("\n");

  return oOo;
}

void
dump_v(oOo_t * oOo, void * null) {
  printf("%s ", (char *) oOo->o);
  return;
}
