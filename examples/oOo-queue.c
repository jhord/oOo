#include <stdio.h>
#include <oOo/oOo.h>

oOo_t ** enq(oOo_t **, char *);
oOo_t ** deq(oOo_t **);
oOo_t ** dump_o(oOo_t **);
void     dump_v(oOo_t *, void *);

int
main(int argc, char ** argv) {
  oOo_t * queue;

  // are we did lisp?
  deq(deq(deq(
    enq(
      enq(
        enq(
          &queue,
          (void *) "queue 1"),
        (void *) "queue 2"),
      (void *) "queue 3")
  )));

  enq(&queue, "queue 1");
  enq(&queue, "queue 2");
  deq(&queue);
  enq(&queue, "queue 3");
  deq(&queue);
  deq(&queue);

  return 0;
}

oOo_t **
enq(oOo_t ** oOo, char * val) {
  /*
    adds an item to the queue.

    add must be used to ensure order just like in mathematics.  either
    the 'n' or the 'p' slot can be used but the opposite slot must
    be used for the dequeueing operation.
  */
  oOo_add_p(oOo, (void *) val);
  //oOo_add_n(oOo, (void *) val);

  printf("enq (%s)\n", val);
  return dump_o(oOo);
}

oOo_t **
deq(oOo_t ** oOo) {
  /*
    removes an item from the queue.

    since a queue is ordered we use cut to move the head pointer.  note
    that you must use the opposite slot to the enqueuing operation.
  */
  char * val = (char *) oOo_cut_n(oOo);
  //char *val = (char *) oOo_cut_p(oOo);

  printf("deq (%s)\n", val);
  return dump_o(oOo);
}

oOo_t **
dump_o(oOo_t ** oOo) {
  printf("  n: ");
  oOo_each_n(*oOo, dump_v, NULL);
  printf("\n");

  printf("  p: ");
  oOo_each_p(*oOo, dump_v, NULL);
  printf("\n");

  return oOo;
}

void
dump_v(oOo_t * oOo, void * null) {
  printf("%s ", (char *) oOo->o);
  return;
}
